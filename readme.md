# Name: Luis Miguel Sánchez Calderón

| References |
| ------  |
| https://jobs.backbonesystems.io/challenge/1 |
| https://jobs.backbonesystems.io/challenge-fullstack.pdf |

# Server Requirements

## Enviroment
| Need to have |
| ------  |
| Docker|
| MySql 8.0.28 |

## Execute
```sh
docker-compose build
docker-compose up -d
```

## Run Migrations
```sh
docker-compose exec -T php php artisan migrate
```

## Run Seeders
```sh
docker-compose exec -T php php artisan db:seed --class=ZipCodeSeeder
```

## Test
```sh
 docker-compose exec -T php php artisan verify:zipCode {zipCode}
```
