<?php

namespace App\Console\Commands;

use App\Http\Services\ZipCodeService;
use Illuminate\Console\Command;

class ZipCodeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify:zipCode {argument}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify zip code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $zipCodeService;

    public function __construct(ZipCodeService $zipCodeService)
    {
        $this->zipCodeService = $zipCodeService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $exec = $this->zipCodeService->test($this->argument('argument'));
        if($exec['status'] == 'success'){
            $this->info("Data is correct");
        }else{
            $this->info("Error {$exec['msg']}");
        }
    }
}
