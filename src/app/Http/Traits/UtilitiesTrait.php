<?php

namespace App\Http\Traits;

trait UtilitiesTrait {
    public function skipAcents($string)
    {
        $string = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
        return str_replace("'", '', $string);
    }
}
