<?php

namespace App\Http\Services;

use App\Http\Traits\UtilitiesTrait;
use App\ZipCode;
use Illuminate\Http\Request;

class ZipCodeService
{

    use UtilitiesTrait;

    public $testEndpoint = 'https://jobs.backbonesystems.io/api/zip-codes/';
    public $testLimit = 3;

    public function get($code)
    {
        $zipCodes = ZipCode::select([
            'd_codigo',
            'd_asenta',
            'd_tipo_asenta',
            'd_municipio',
            'd_estado',
            'd_ciudad',
            'c_estado',
            'c_municipio',
            'id_asenta_cpcons',
            'd_zona'
        ])->where('d_codigo', 'LIKE', '%'.$code.'%')
            ->get();

        $data['zip_code'] = $zipCodes[0]->d_codigo;
        $data['locality'] = strtoupper($this->skipAcents($zipCodes[0]->d_ciudad));
        $data['federal_entity'] = [
            'key'=>intval($zipCodes[0]->c_estado),
            'name'=>strtoupper($this->skipAcents($zipCodes[0]->d_estado)),
            'code'=>null
        ];
        foreach ($zipCodes as $zipCode){
            $settlements[] = [
                'key'=>intval($zipCode->id_asenta_cpcons),
                'name'=>strtoupper($this->skipAcents($zipCode->d_asenta)),
                'zone_type'=>strtoupper($this->skipAcents($zipCode->d_zona)),
                'settlement_type'=>[
                    'name'=>$zipCode->d_tipo_asenta
                ]
            ];
        }
        $data['settlements'] = $settlements;
        $data['municipality'] = [
            'key'=>intval($zipCodes[0]->c_municipio),
            'name'=>strtoupper($this->skipAcents($zipCodes[0]->d_municipio))
        ];

        return $data;
    }
    public function test($code)
    {
        $testCode = $this->get($code);
        $testNumber = 0;

        do {
            $testNumber++;
            $file = @file_get_contents($this->testEndpoint . $code);
        } while (empty($file) && $testNumber < $this->testLimit);

        if (!empty($file)){

            $originalCode = json_decode($file, true);

            if ($testCode == $originalCode){
                return [
                    'status'=>'success'
                ];
            } else {
                return [
                    'status'=>'error',
                    'msg'=>'zip code content is invalid'
                ];
            }

        } else {
            return [
                'status'=>'error',
                'msg'=>'zip code endpoint is invalid'
            ];
        }
    }
}
