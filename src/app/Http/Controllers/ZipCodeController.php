<?php

namespace App\Http\Controllers;

use App\Http\Services\ZipCodeService;
use Illuminate\Http\Request;

class ZipCodeController extends Controller
{
    public $zipCodeService;

    public function __construct(ZipCodeService $zipCodeService)
    {
        $this->zipCodeService = $zipCodeService;
    }
    public function get($code)
    {
        $data = $this->zipCodeService->get($code);

        return response()->json($data, 200);
    }
}
