<?php

use Illuminate\Database\Seeder;

class ZipCodeSeeder extends Seeder
{
    /**
     * php artisan db:seed --class=ZipCodeSeeder
     */
    public function run()
    {
        DB::table('zip_codes')->truncate();
        $file = fopen(dirname(__FILE__)."/CPdescarga.txt", "r");
        $line = 0;
        $skip = 2;
        while(!feof($file)) {
            $newFile = preg_replace( "/\r|\n/", "", fgets($file));
            $skip--;
            if($skip < 0){
                $newFile = explode('|',$newFile);
                $line++;
                DB::table('zip_codes')->insert([
                    [
                        'd_codigo'=>utf8_encode($newFile[0]),
                        'd_asenta'=>utf8_encode($newFile[1]),
                        'd_tipo_asenta'=>utf8_encode($newFile[2]),
                        'd_municipio'=>utf8_encode($newFile[3]),
                        'd_estado'=>utf8_encode($newFile[4]),
                        'd_ciudad'=>utf8_encode($newFile[5]),
                        'd_cp'=>utf8_encode($newFile[6]),
                        'c_estado'=>utf8_encode($newFile[7]),
                        'c_oficina'=>utf8_encode($newFile[8]),
                        'c_cp'=>utf8_encode($newFile[9]),
                        'c_tipo_asenta'=>utf8_encode($newFile[10]),
                        'c_municipio'=>utf8_encode($newFile[11]),
                        'id_asenta_cpcons'=>utf8_encode($newFile[12]),
                        'd_zona'=>utf8_encode($newFile[13]),
                        'c_cve_ciudad'=>utf8_encode($newFile[14])
                    ]
                ]);
            }
        }
        if($line == DB::table('zip_codes')->count()){
            print('success ');
        }
        fclose($file);
    }
}
