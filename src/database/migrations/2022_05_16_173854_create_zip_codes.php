<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZipCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zip_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('d_codigo');
            $table->text('d_asenta');
            $table->text('d_tipo_asenta');
            $table->text('d_municipio');
            $table->text('d_estado');
            $table->text('d_ciudad');
            $table->text('d_cp');
            $table->text('c_estado');
            $table->text('c_oficina');
            $table->text('c_cp');
            $table->text('c_tipo_asenta');
            $table->text('c_municipio');
            $table->text('id_asenta_cpcons');
            $table->text('d_zona');
            $table->text('c_cve_ciudad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zip_codes');
    }
}
